#!/bin/bash
VERSION="0.0"
tmp=`grep -e "__version__" ../pgext/__init__.py`
tmp=${tmp// /}
tmp=${tmp//\"/}
VERSION=${tmp//__version__=/}

cd ..
rm examples/*.pyc

tar -zcf pgext-examples-$VERSION.tar.gz examples/*
zip -r pgext-examples-$VERSION.zip examples/*

tar -zcf pgext-docs-$VERSION.tar.gz doc/*
zip -r pgext-docs-$VERSION.zip doc/*
