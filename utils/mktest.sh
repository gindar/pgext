#!/bin/bash
cd ..
echo '--- Building ---'
python setup.py build
cp -R build/lib*/pgext test/
echo '--- Running tests ---'
cd test
rm out_*.png
python test.py
cd ..
echo '--- Cleanup ---'
rm -Rf test/pgext/
rm -Rf build/
