#!/bin/bash
VERSION="0.0"
tmp=`grep -e "__version__" ../pgext/__init__.py`
tmp=${tmp// /}
tmp=${tmp//\"/}
VERSION=${tmp//__version__=/}

cd ..
cg2make doc/*.cgd --outpath=doc --version=$VERSION
