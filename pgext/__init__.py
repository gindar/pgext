# coding=utf_8
'''
    PGExt - pygame extension
    Created by Josef Vanžura <gindar@zamraky.cz>
'''

__version__ = "1.18"

import filters
import color
import ibi
