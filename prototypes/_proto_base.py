import pygame


class PrototypeBase:
    def __init__(self, image, fps=60):
        if image:
            self.image = pygame.image.load(image)
            self.size = self.image.get_size()
        else:
            self.image = None
            self.size = (256, 256)
        self.fps = fps
        self.screen = pygame.display.set_mode(self.size)
        self.running = True
        self.loop()

    def processEvent(self, e):
        pass

    def process(self):
        pass

    def loop(self):
        self.running = True
        clock = pygame.time.Clock()
        while self.running:
            print clock.tick(self.fps)
            for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    self.running = False
                    break
                else:
                    self.processEvent(e)
            self.process()
            pygame.display.flip()
