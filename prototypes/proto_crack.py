import math
import random
from _proto_base import PrototypeBase


class Crack(PrototypeBase):

    def process(self):
        image = self.image.copy()
        w, h = image.get_size()

        max_size = 0.1
        move_offset_x = 10
        move_offset_y = 10
        number = 1000

        w_base = (w * max_size * 0.1)
        w_size = ((w * max_size) - w_base)
        h_base = (h * max_size * 0.1)
        h_size = ((h * max_size) - h_base)

        for n in xrange(number):
            iw = w_base + (random.random() * w_size)
            ih = h_base + (random.random() * h_size)
            ox = (random.random() * move_offset_x * 2) - move_offset_x
            oy = (random.random() * move_offset_x * 2) - move_offset_y
            x = random.random() * (w - iw)
            y = random.random() * (h - ih)
            image.blit(image, (x + ox, y + oy), (x, y, iw, ih))

        self.screen.blit(image, (0, 0))

proto = Crack("source_rgb.png", 60)
