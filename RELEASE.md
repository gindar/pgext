
 * Change version in __init__.py
 * Check CHANGES.md and update it
 * Check README.md (links)
 * make and check docs
 * make examples & docs packages
 * Build & install linux version. Run tests test/test.py
 * Build & install windows version. Run tests test/test.py
 * Windows: run mkdist_win.bat
 * Commit Release <VERSION> & push
 * Upload docs & examples to web
 * Upload windows packages (exe, zip) to web
 * Upload docs to python hosted
 * Change web version
 * Create source package & upload it to pypy: python setup.py sdist upload
 * Create release on pygame.org (changes, check links)
 * Announce on FB and Twitter
