'''
    pgext - filters.noise example
'''
import pygame
import pgext
import time
from _example import ExampleBase


class ExampleApp(ExampleBase):
    def load(self):
        self.image = pygame.image.load("data/lena.png").convert()

    def draw(self, tdelta):
        ''' Drawing function '''
        image = self.image.copy()
        pgext.color.setColor(image, (255,0,0, 80))
        self.screen.blit(image, (0, 0))

app = ExampleApp((256, 256))
app.loop()
