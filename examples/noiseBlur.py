'''
    pgext - filters.noiseBlur example
'''
import pygame
import pgext
from _example import ExampleBase


class ExampleApp(ExampleBase):
    def load(self):
        self.image = pygame.image.load("data/lena.png").convert()

    def draw(self, tdelta):
        ''' Drawing function '''
        image = self.image.copy()
        pgext.filters.noiseBlur(image, 4)
        self.screen.blit(image, (0, 0))

app = ExampleApp((256, 256))
app.loop()
