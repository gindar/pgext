'''
    pgext - example base
'''
import pygame


class ExampleBase:
    def __init__(self, resolution, fps=60):
        pygame.display.init()
        self.running = False
        self.fps = fps
        self.screen = pygame.display.set_mode(resolution)
        self.t = 0
        self.load()

    def load(self):
        ''' Load resources '''
        pass

    def draw(self, tdelta):
        ''' Drawing function '''
        pass

    def loop(self):
        ''' Game loop '''
        self.running = True
        clock = pygame.time.Clock()
        while self.running:
            tdelta = clock.tick(self.fps)
            for e in pygame.event.get():
                if e.type == pygame.QUIT:
                    self.running = False
                    break
            self.t += tdelta
            self.draw(tdelta)
            pygame.display.flip()
