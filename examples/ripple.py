'''
    pgext - ripple example
'''
import pygame
import pgext
from _example import ExampleBase


class ExampleApp(ExampleBase):
    def load(self):
        self.image = pygame.image.load("data/dirt.png").convert()

    def draw(self, tdelta):
        ''' Drawing function '''
        q = (self.t % 1000) / 1000.0
        image = self.image.copy()
        pgext.filters.ripple(image, 100, 5, q)
        pgext.filters.ripple(image, 80, 5, q, 1)
        self.screen.blit(image, (0, 0))

app = ExampleApp((500, 200))
app.loop()
