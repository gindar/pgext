'''
    pgext - filters.pixelize example
'''
import math
import pygame
import pgext
from _example import ExampleBase


class ExampleApp(ExampleBase):
    def load(self):
        self.image = pygame.image.load("data/lena.png").convert()

    def draw(self, tdelta):
        ''' Drawing function '''
        q = (self.t % 5000) / 5000.0
        image = self.image.copy()
        n = abs(math.sin(q * math.pi) * 10)
        if n > 1:
            pgext.filters.pixelize(image, int(n))
        self.screen.blit(image, (0, 0))

app = ExampleApp((256, 256))
app.loop()
