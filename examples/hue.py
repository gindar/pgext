'''
    pgext - hue example
'''
import pygame
import pgext
from _example import ExampleBase


class ExampleApp(ExampleBase):
    def load(self):
        self.image = pygame.image.load("data/lena.png").convert()

    def draw(self, tdelta):
        ''' Drawing function '''
        q = (self.t % 3000) / 3000.0
        image = self.image.copy()
        pgext.color.hue(image, int(q * 360))
        self.screen.blit(image, (0, 0))

app = ExampleApp((256, 256))
app.loop()
